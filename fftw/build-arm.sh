#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

BUILD_THREADS=16 ARCH_OPTS=--enable-neon "${script_dir}/build.sh" "$@"
