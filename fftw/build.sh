#!/bin/bash

set -eu
set -o pipefail

if [ $# -lt 2 ]; then
    echo "Usage: build.sh /path/to/sources /path/to/prefix"
    exit 1
fi
src="$1"
prefix="$2"

echo "Using prefix: $prefix"

if [ -n "${ARCH_OPTS:-}" ]; then
    mapfile -t arch_opts <<< "$ARCH_OPTS"
fi


cd "$src"

read -p "This is a multi-stage build which will need to run 'make install'. Continue with PREFIX=$prefix? [y/N] " -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Abort."
    exit 2
fi

[ -f Makefile ] && make clean
eval ./configure "${arch_opts[@]}" --enable-shared --enable-openmp --enable-mpi --with-pic=yes --prefix="$prefix"
make -j "${BUILD_THREADS:-8}"
make install

echo
echo "Built for double-precision. Continuing with single precision..."
echo

make clean
eval ./configure "${arch_opts[@]}" --enable-single --enable-shared --enable-openmp --enable-mpi --with-pic=yes --prefix="$prefix"
make -j "${BUILD_THREADS:-8}"
make install

echo
echo "Done."
