#!/bin/bash

set -eu
set -o pipefail

FFTW_VERSION="${FFTW_VERSION:-3.3.10}"
SHA256="${SHA256:-56c932549852cddcfafdab3820b0200c7742675be92179e59e6215b340e26467}"

# Fetch
tarname="fftw-$FFTW_VERSION.tar.gz"

if [ ! -r "$tarname" ]; then
    url="http://www.fftw.org/$tarname"
    wget "$url"
fi

actual_sha256="$(sha256sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha256" != "$SHA256" ]; then
    echo "Checksum does not match."
    echo "$SHA256 expected"
    echo "$actual_sha256 computed"
    exit 1
fi

tar xf "$tarname"

echo 'Now run `build.sh $PWD/'"fftw-${FFTW_VERSION}"' PREFIX`'
