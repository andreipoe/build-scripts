#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

BUILD_THREADS="${BUILD_THREADS:-64}" ARCH_OPTS='--enable-sse2 --enable-avx --enable-avx2' "${script_dir}/build.sh" "$@"
