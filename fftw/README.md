# FFTW


This downloads FFTW from [their website](http://www.fftw.org/).
The current release used is 3.3.10.

To download sources:

    ./fetch.sh

There is a single command to build all version of the library

```bash
./build.sh /path/to/sources # Builds for `$HOME/local/packages`
./build.sh /path/to/sources /path/to/prefix
PREFIX=/path/to/prefix CC=armclang CXX=armclang++ ./build.sh /path/to/sources
```

**Note**: This script will _automatically run `make install`_, beause the package is built in two steps (single and double precision) that require reconfiguring.

