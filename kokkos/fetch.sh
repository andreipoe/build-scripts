#!/bin/bash

set -eu

targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

git clone --depth 1 'https://github.com/kokkos/kokkos.git'

echo "Done."

