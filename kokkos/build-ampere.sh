#!/bin/bash

set -eu

if [ $# -ge 2 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh <source-dir> <install-dir>." >&2
        exit 1
    fi

    sourcedir="$(readlink -f "$1")"
    installdir="$(readlink -m "$2")"
else
    echo "Usage: build.sh <source-dir> <install-dir>." >&2
    exit 1
fi


if [ ! -d "$sourcedir" ]; then
    echo "Source directory does not exist: $sourcedir" >&2
    exit 2
fi

echo "Install directory: $installdir"

cd "$sourcedir"

mkdir build-ampere
cd build-ampere
../generate_makefile.bash --prefix="$installdir" --with-openmp --with-pthread --arch=armv8-a --compiler=gcc

time make -j |& tee build.log
make install

echo "Done."

