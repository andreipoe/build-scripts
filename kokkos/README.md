# Kokkos

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
```

This clones the master branch (latest release) of [the Kokkkos GitHub repo](https://github.com/kokkos/kokkos).

## Build 

```bash
./build.sh /sources/dir/ /install/dir/

```

Note that you _need to_ specify all the script parameters above.

The options used are (taken from [the BabelStream instructions](https://github.com/UoB-HPC/BabelStream)):

```
../generate_makefile.bash --prefix=/install/dir --with-openmp --with-pthread --arch=<arch> --compiler=gcc
```

### Architectures

The following architecture flags are used:

| Build target | `arch`    |
| ------------ | --------- |
| Ampere       | `armv8-a` |

