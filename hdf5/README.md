# HDF5

[Instructions on the Arm Package Wiki](https://gitlab.com/arm-hpc/packages/-/wikis/packages/hdf5).

## Default build

The default version is 1.12.2.

    ./fetch.sh
    ./build.sh SRC_DIR INSTALL_DIR

The default flags are:

    --enable-parallel --enable-fortran --with-zlib --with-pic --prefix=$install_dir

Note that [you need to make manual changes to use ACfL](https://gitlab.com/arm-hpc/packages/-/wikis/packages/hdf5#build-details-for-version-1105-with-arm-compiler-for-hpc-200).

## Custom build

Use `HDF5_VERSION` to fetch a different release:

    HDF5_VERSION=... ./fetch.sh

Pass extra configure options with `EXTRA_ARGS`:

    EXTRA_ARGS=... ./build.sh SRC_DIR INSTALL_DIR

`CC`, `FC`, and `CXX` -- which should point to your MPI compilers -- are forwarded. The defaults are `mpicc`, `mpif90`, and `mpicxx`.

`CFLAGS`, `CXXFLAGS`, `FCFLAGS`, and `LDFLAGS` are untouched.
