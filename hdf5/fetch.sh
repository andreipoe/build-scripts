#!/bin/bash

set -eu

version="${HDF5_VERSION:-1.12.2}"
major="${version%.*}"

wget "https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-${major}/hdf5-${version}/src/hdf5-${version}.tar.bz2"

echo "Unpacking..."
tar xf "hdf5-${version}.tar.bz2"

echo "Done."
