#!/bin/bash

set -eu

if [ "$#" -lt 2 ]; then
    echo "Usage: build.sh SRC_DIR INSTALL_DIR"
    exit 1
fi

src="$1"
dest="$2"

cd "$src"
echo "Installing to: $dest"

args=( --enable-parallel --enable-fortran --with-zlib --with-pic )

if [ -n "${EXTRA_ARGS:-}" ]; then
    mapfile -t extra_args <<< "$EXTRA_ARGS"
    args+=( "${extra_args[@]}" )
fi

[ -f Makefile ] && make clean
CC=${CC:-mpicc} FC=${FC:-mpif90} CXX=${CXX:-mpicxx} ./configure "${args[@]}" "--prefix=$dest"

make -j ${BUILD_THREADS:-16}
make install

echo
echo "Done."
