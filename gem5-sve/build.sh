#!/bin/bash

set -eu
set -o pipefail

JOBS="${JOBS:-8}"

scons_dir="$(find . -maxdepth 1 -type d -name "scons-local-*" | tail -1)"
if [ -z ${scons_dir:-} ]; then
    echo "Cannot find scons-local. Have you run fetch.sh?"
    exit 1
fi

echo "Using $JOBS parallel jobs"
python "$scons_dir"/scons.py build/ARM/gem5.opt -j "$JOBS"



