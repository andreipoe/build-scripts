# Gem5 SVE

## Fetch

```bash
./fetch.sh # Uses `$PWD`
```

This downloads Gem5 from [Google Source](https://gem5.googlesource.com/arm/gem5). The current branch used is `sve/beta1`, but this can be overridden:

```bash
GEM5_BRANCH='sve/beta0' ./fetch.sh
```

To avoid the dependency on scons, a copy of scons-local is downloaded from [the scons website](http://www.scons.org/pages/download.html). The current release used is 3.0.1, but this can be overridden:

```bash
SCONS_VERSION=3.0.0 ./fetch.sh
```

## Build 

```bash
./build.sh
JOBS=64 ./build.sh # Set the number of parallel jobs
```

Building requires Python 2 with the development headers. See [`python-2.7`](../python-2.7) for help building Python 2.7.

