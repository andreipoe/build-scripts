#!/bin/bash

set -eu
set -o pipefail

GEM5_BRANCH="${GEM5_BRANCH:-sve/beta1}"
SCONS_VERSION="${SCONS_VERSION:-3.0.1}"
JOBS="${JOBS:-8}"

echo "Using gem5 branch $GEM5_BRANCH"
git clone -b "$GEM5_BRANCH" --depth 1 https://gem5.googlesource.com/arm/gem5

echo "Using scons version $SCONS_VERSION"
scons_dir="scons-local-$SCONS_VERSION"
scons_tar="${scons_dir}.tar.gz"
wget "http://prdownloads.sourceforge.net/scons/${scons_tar}"

mkdir "$scons_dir"
(cd "$scons_dir"; tar xf "../$scons_tar")

echo "Done."

