# Stow

## Fetch

```bash
./fetch.sh # Uses `$PWD`
```

This downloads Stow from [the Kent GNU mirror](https://www.mirrorservice.org/sites/ftp.gnu.org/gnu/stow/). The current release used is 2.2.0.

## Build 

```bash
./build.sh # Builds for `$PROJ`
./build.sh /path/to/prefix
PREFIX=/path/to/prefix ./build.sh
```

