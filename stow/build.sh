#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules/stow/2.2.2}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"

module purge

./configure --prefix="$PREFIX"

make -j16

cat <<EOF

To install to "$PREFIX":
    make install

Done.
EOF

