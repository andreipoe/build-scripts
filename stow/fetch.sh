#!/bin/bash

set -eu
set -o pipefail

STOW_VERSION="${STOW_VERSION:-2.2.2}"
SHA256="${SHA256:-a0022034960e47a8d23dffb822689f061f7a2d9101c9835cf11bf251597aa6fd}"

tarname="stow-$STOW_VERSION.tar.bz2"
url="https://www.mirrorservice.org/sites/ftp.gnu.org/gnu/stow/$tarname"
wget "$url"
actual_sha256="$(sha256sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha256" != "$SHA256" ]; then
    echo "Checksum does not match."
    echo "$SHA256 expected"
    echo "$actual_sha256 computed"
    exit 1
fi

tar xf "$tarname"

echo "Done."

