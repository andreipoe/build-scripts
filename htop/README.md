# htop


This downloads htop from [the release page](https://hisham.hm/htop/releases/).
The current release used is 2.2.0.
The script is _architecture-independent_.

There is a single command to build and run:

```bash
./build.sh # Builds for `$HOME/local/packages`
./build.sh /path/to/prefix
PREFIX=/path/to/prefix ./build.sh
```

