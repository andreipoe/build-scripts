#!/bin/bash

set -eu
set -o pipefail

HTOP_VERSION="${HTOP_VERSION:-3.0.2}"
PREFIX="${PREFIX:-$HOME/local/packages/htop-$HTOP_VERSION}"
SHA256="${SHA256:-6471d9505daca5c64073fc37dbab4d012ca4fc6a7040a925dad4a7553e3349c4}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"


# Fetch
tarname="htop-$HTOP_VERSION.tar.gz"

if [ ! -r "$tarname" ]; then
    curl -JLO "https://bintray.com/htop/source/download_file?file_path=htop-${HTOP_VERSION}.tar.gz"
fi

actual_sha256="$(sha256sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha256" != "$SHA256" ]; then
    echo "Checksum does not match."
    echo "$SHA256 expected"
    echo "$actual_sha256 computed"
    exit 1
fi

tar xf "$tarname"


# Build
cd "${tarname%.tar.gz}"

./configure --prefix="$PREFIX"

make -j16

cat <<EOF

To install to "$PREFIX":
    make install

Done.
EOF

