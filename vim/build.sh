#!/bin/bash

set -eu

prefix="${PREFIX:-}"
[ $# -ge 1 ] && prefix="$1"

if [ -n "${prefix:-}" ]; then
    echo "Usage: build.sh PREFIX"
    exit 1
fi

[ "$BUILD_STATIC" = "yes" ] && export LDFLAGS="-static"
./configure --disable-channel --disable-gpm --disable-gtktest --disable-gui --disable-netbeans --disable-nls --disable-selinux --disable-smack --disable-sysmouse --disable-xsmp --enable-multibyte --with-features=huge --without-x --with-tlib=ncursesw --with-compiledby='Drone <drone@simd.stream>' --prefix="$prefix"

make -j

cat <<EOF

To install to "$prefix":
    make install

Done.
EOF
