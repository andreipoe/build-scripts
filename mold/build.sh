#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-$HOME/local/packages/mold}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"


# Build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$PREFIX" -B build
cmake --build build -j "${BUILD_THREADS:-4}"
cmake --build build --target install

echo
echo "Done."
