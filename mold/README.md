# mold


This downloads the latest stable release of mold from [the GitHub repo](https://github.com/rui314/mold).
The script is _architecture-independent_.

```bash
./build.sh # Builds for `$HOME/local/packages`
./build.sh /path/to/prefix
PREFIX=/path/to/prefix ./build.sh
```

