#!/usr/bin/env bash

set -eu
set -o pipefail

if [ $# -lt 2 ]; then
    echo "Usage: build.sh <sources> <destination>"
    exit 1
fi

src="$1"
dest="$2"
shift 2

if ! [ -d "$src" ]; then
    echo "No such source directory: $src"
    exit 2
fi

cd "$src"
rm -rf build
mkdir build
cd build

cmake -DCMAKE_INSTALL_PREFIX="$dest" -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_PROJECTS="clang;openmp;lld" -G Ninja "$@" ../llvm

ninja
ninja install

