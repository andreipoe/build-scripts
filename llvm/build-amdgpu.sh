#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

if [ $# -lt 2 ]; then
    echo "Usage: build.sh <sources> <destination>"
    exit 1
fi

src="$1"
dest="$2"
shift 2

"${script_dir}/build.sh" "$src" "$dest" -DLLVM_ENABLE_PROJECTS="clang;openmp;lld" -DLLVM_ENABLE_ASSERTIONS=OFF -DLLVM_ENABLE_DUMP=OFF "$@"
