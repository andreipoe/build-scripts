# LLVM

This builds LLVM with the common HPC components.
The current version used is 14.0.6; set `VERSION` to override.

## Usage


```
./fetch.sh [/path/to/target/dir] # default: $PWD

./build.sh </path/to/sources> </path/to/install/dir> [ARGS...]
```

Any `ARGS` given are passed to `cmake`.
