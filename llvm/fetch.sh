#!/usr/bin/env bash

set -eu
set -o pipefail

ver="${VERSION:-14.0.6}"

declare -A sha256
sha256[11.0.0]='b7b639fc675fa1c86dd6d0bc32267be9eb34451748d2efd03f674b773000e92b'
sha256[14.0.6]='8b3cfd7bc695bd6cea0f37f53f0981f34f87496e79e2529874fd03a2f9dd3a8a'

fname="llvm-project-${ver}.src.tar.xz"

dest="$PWD"
if [ $# -ge 1 ]; then
    if ! [ -d "$1" ]; then
        echo "No such directory: $1"
        exit 1
    fi

    dest="$1"
    cd "$dest"
fi

rm -f "$fname"
wget "https://github.com/llvm/llvm-project/releases/download/llvmorg-${ver}/${fname}"

sha256_actual="$(sha256sum "$fname" | awk '{print $1}')"
if ! grep -q "${sha256[$ver]}" <<<"${sha256_actual}"; then
    echo "Invalid checksum:"
    echo "  expected: $fname"
    echo "  actual:   ${sha256_actual}"
    echo "Stop."
    exit 2
fi
echo "Checksum OK"

echo -n "Unpacking... "
tar xf "$fname"
echo "DONE."
