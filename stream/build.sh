#!/bin/bash

set -eu

COMPILER="${COMPILER:-GNU}"
CC="${CC:-gcc}"

targetdir="$PWD"
fname="stream"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh [<target-dir> [<executable-name>]]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi

    if [ $# -ge 2 ]; then
        fname="$2"
    fi
fi

echo "Target directory: $targetdir"

outfile="$targetdir/$fname"
if [ -f "$outfile" ]; then
    echo "File already exists: $outfile" >&2
    exit 3
else
    echo "Building into: $outfile"
fi

echo "Compiler (set COMPILER and CC to override): $COMPILER ($CC)"

cflags=( -Ofast -fopenmp -ffast-math )
case "$(uname -m)" in
    aarch64)
        cflags+=( "-mcpu=${ARCH:-thunderx2t99}" -ffp-contract=fast "-DSTREAM_ARRAY_SIZE=${ARRAY_SIZE:-16777216}" "-DNTIMES=${NTIMES:-10}" )
        ;;
    x86*)
        cflags+=( -mcmodel=medium "-march=${ARCH:-native}" "-DSTREAM_ARRAY_SIZE=${ARRAY_SIZE:-105000000}" "-DNTIMES=${NTIMES:-500}" )

        case "$COMPILER" in
            cray|CRAY|cce|CCE)
                cflags+=( -floop-trips=huge -flocal-restrict )
                ;;
            aocc|AOCC|amd|AMD)
                cflags+=( -fnt-store )
                ;;
        esac
        ;;
esac
cflags+=( ${CFLAGS:-} )

set -x
"$CC" "${cflags[@]}" -o "$outfile" stream.c
set +x

echo "Done."

