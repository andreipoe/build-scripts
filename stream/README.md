# STREAM 

Both scripts take `-h` or `--help` to show the usage.

## Fetch

```bash
./fetch.sh /path/to/target # default is $PWD
```

This will download `stream.c` from [its homepage](https://www.cs.virginia.edu/stream/FTP/Code/stream.c).

## Build

By default, gcc is used.
Additional supported compilers are: AOCC, CRAY.

```bash
CC=clang COMPILER=AOCC ./build.sh /path/to/target/dir/
```

Manually specify the pahth:

```bash
./build.sh /path/to/target/dir/
./build.sh /path/to/target/dir/ executable.name $ default is `$PWD` and `stream`
```

Set the compiler or *add* flags:

```bash
CC=armclang ./build.sh # default is `gcc`
CFLAGS="-ffp-contract=fast" ./build.sh
```

You can also set the following STREAM parameters: ARRAY_SIZE, NTIMES:

```bash
NTIMES=100 ARRAY_SIZE=$((2 ** 32)) ./build.sh
```

