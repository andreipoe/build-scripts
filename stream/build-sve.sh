#!/bin/bash

set -eu

CC="${CC:-gcc}"
targetdir="$PWD"
fname="stream.sve"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh [<target-dir> [<executable-name>]]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi

    if [ $# -ge 2 ]; then
        outfile="$2"
    fi
fi

echo "Target directory: $targetdir"

outfile="$targetdir/$fname"
if [ -f "$outfile" ]; then
    echo "File already exists: $outfile" >&2
    exit 3
else
    echo "Building into: $outfile"
fi

echo "Compiler (set CC to override): $CC"

$CC -march=armv8.1-a+sve -O3 -ffast-math -DSTREAM_ARRAY_SIZE=4194304 -DNTIMES=2 -o "$outfile" stream.c

echo "Done."

