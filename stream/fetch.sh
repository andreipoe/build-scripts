#!/bin/bash

set -eu

targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"

outfile="$targetdir/stream.c"
if [ -f "$outfile" ]; then
    echo "File already exists: $outfile" >&2
    exit 4
fi

wget -O "$outfile"  https://www.cs.virginia.edu/stream/FTP/Code/stream.c

expectedh="a52bae5e175bea3f7832112af9c085adab47117f7d2ce219165379849231692b"
actualh=$(sha256sum "$outfile" | awk '{print $1}')
if [ "$actualh" != "$expectedh" ]; then
    echo "Checksum does NOT match for: $outfile"
    echo "Expected: $expectedh"
    echo "Actual: $actualh"
    exit 3
fi

echo "Done."

