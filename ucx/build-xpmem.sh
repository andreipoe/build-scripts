#!/bin/bash

BUILD_THREADS="${BUILD_THREADS:-16}"
IFS=' ' read -r -a PLATFORM_OPTS <<<"${PLATFORM_OPTS:-}"

set -eu

if [ $# -ge 2 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh <source-dir> <install-dir>." >&2
        exit 1
    fi

    sourcedir="$(readlink -f "$1")"
    installdir="$(readlink -m "$2")"
else
    echo "Usage: build.sh <source-dir> <install-dir>." >&2
    exit 1
fi


if [ ! -d "$sourcedir" ]; then
    echo "Source directory does not exist: $sourcedir" >&2
    exit 2
fi

echo "Install directory: $installdir"

cd "$sourcedir"

./contrib/configure-release --enable-mt --enable-optimizations --with-avx --enable-cma --without-gdrcopy --with-verbs --without-knem --without-rdmacm --without-rocm --with-xpmem --without-ugni --without-java --without-cuda --prefix="$installdir" "${PLATFORM_OPTS[@]}"

time make -j "$BUILD_THREADS" |& tee build.log
make install

echo "Done."

