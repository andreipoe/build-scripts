#!/bin/bash

set -eu

#pkgver="1.13.1"; sha256sum="2c4a2f96c700e3705e185c2846a710691b6e800e8aec11fd4b3e47bcc3990548"
pkgver="1.15.0"; sha256sum="4b202087076bc1c98f9249144f0c277a8ea88ad4ca6f404f94baa9cb3aebda6d"
targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

pkgtar="ucx-${pkgver}.tar.gz"
wget "https://github.com/openucx/ucx/releases/download/v${pkgver}/${pkgtar}"

actualh=$(sha256sum "$pkgtar" | awk '{print $1}')
if [ "$actualh" != "$sha256sum" ]; then
    echo "Checksum does NOT match for: $pkgtar"
    echo "Expected: $sha256sum"
    echo "Got: $actualh"
    exit 3
fi

if [ -d "openmpi-$pkgver" ]; then
    echo "Cannot extract: directory already exists: openmpi-$pkgver" >&2
    exit 4
fi

echo "Extrcting $pkgtar..."
tar xf "$pkgtar"

echo "Done."

