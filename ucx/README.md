# UCX

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
```

This downloads UCX from [GitHub Releases](https://github.com/openucx/ucx/releases/tag/v1.13.1). The current release used is 1.13.1.

## Build 

Set `CC` and `CXX` to specify compilers.

```bash
./build.sh /sources/dir/ /install/dir/
CC=gcc CXX=g++ ./build.sh /sources/dir/ /install/dir/
```

Note that you _need to_ specify all the script parameters above.

The following options are used:

```
./contrib/configure-release --enable-mt --with-knem --enable-optimizations --with-avx --enable-cma --without-gdrcopy --with-verbs --with-knem --with-rdmacm --without-rocm --without-xpmem --without-ugni --without-java --without-cuda
```

