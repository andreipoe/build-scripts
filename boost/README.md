# Boost

This builds Boost following the [official instructions](https://www.boost.org/doc/libs/1_74_0/more/getting_started/unix-variants.html#the-boost-distribution).
The current version used is 1.87.0; set `VERSION` to override.

## Usage


```
./fetch.sh [/path/to/target/dir] # default: $PWD

./build.sh </path/to/sources> </path/to/install/dir> [ARGS...]
```

Any `ARGS` given are passed to `bootstrap.sh`.
For example, to build the HipSYCL libraries:

    ./build.sh ... ... --with-libraries=fiber,context,test
