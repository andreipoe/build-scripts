#!/usr/bin/env bash

set -eu
set -o pipefail

if [ $# -lt 2 ]; then
    echo "Usage: build.sh <sources> <destination>"
    exit 1
fi

src="$1"
dest="$2"
shift 2

if ! [ -d "$src" ]; then
    echo "No such source directory: $src"
    exit 2
fi

cd "$src"
eval ./bootstrap.sh --prefix="$dest" "$@"
./b2 install
