#!/usr/bin/env bash

set -eu
set -o pipefail

ver="${VERSION:-1.87.0}"

declare -A fname sha256
fname[1.73.0]='boost_1_73_0.tar.bz2'
sha256[1.73.0]='4eb3b8d442b426dc35346235c8733b5ae35ba431690e38c6a8263dce9fcbb402'
fname[1.81.0]='boost_1_81_0.tar.bz2'
sha256[1.81.0]='71feeed900fbccca04a3b4f2f84a7c217186f28a940ed8b7ed4725986baf99fa'
fname[1.87.0]='boost_1_87_0.tar.bz2'
sha256[1.87.0]='af57be25cb4c4f4b413ed692fe378affb4352ea50fbe294a11ef548f4d527d89'


dest="$PWD"
if [ $# -ge 1 ]; then
    if ! [ -d "$1" ]; then
        echo "No such directory: $1"
        exit 1
    fi

    dest="$1"
    cd "$dest"
fi

rm -f "${fname[$ver]}"
# wget "https://boostorg.jfrog.io/artifactory/main/release/${ver}/source/${fname[$ver]}"
wget "https://archives.boost.io/release/${ver}/source/${fname[$ver]}"

sha256_actual="$(sha256sum "${fname[$ver]}" | awk '{print $1}')"
if ! grep -q "${sha256[$ver]}" <<<"${sha256_actual}"; then
    echo "Invalid checksum:"
    echo "  expected: ${sha256[$ver]}"
    echo "  actual:   ${sha256_actual}"
    echo "Stop."
    exit 2
fi
echo "Checksum OK"

echo -n "Unpacking... "
tar xf "${fname[$ver]}"
echo "DONE."
