#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules/python/2.7.15}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"

module purge
module load gcc

CC=gcc CFLAGS='-O3 -march=core-avx2 -fPIC' ./configure --prefix="$PREFIX" --enable-optimizations --enable-shared

make -j24

cat <<EOF

To install to "$PREFIX":
    make install

Done.
EOF
