#!/bin/bash

set -eu
set -o pipefail

PYTHON_VERSION="${PYTHON_VERSION:-2.7.15}"
MD5="${MD5:-a80ae3cc478460b922242f43a1b4094d}"

tarname="Python-$PYTHON_VERSION.tar.xz"
url="https://www.python.org/ftp/python/${PYTHON_VERSION}/$tarname"
wget "$url"
actual_md5="$(md5sum "$tarname" | awk '{print $1}')"

if [ "$actual_md5" != "$MD5" ]; then
    echo "Checksum does not match."
    echo "$MD5 expected"
    echo "$actual_md5 computed"
    exit 1
fi

tar xf "$tarname"

echo "Done."

