#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules-arm/python/2.7.15}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"

module purge
module load gcc

CC=gcc CFLAGS='-O3 -march=armv8.1-a -mcpu=thunderx2t99' ./configure --prefix="$PREFIX" --enable-optimizations

make -j64

cat <<EOF

To install to "$PREFIX":
    make install

Done.
EOF
