# Python 2.7

## Fetch

```bash
./fetch.sh # Uses `$PWD`
```

This downloads Python from [source releases](https://www.python.org/downloads/source/). The current release used is 2.7.15, but this can be overridden:

```bash
PYTHON_VERSION=2.7.14 MD5=1f6db41ad91d9eb0a6f0c769b8613c5b ./fetch.sh
```

## Build 

```bash
./build.sh # Builds for `$PROJ`
./build.sh /path/to/prefix
PREFIX=/path/to/prefix ./build.sh
```

### ThunderX2

The following options are used:

```
-O3 -march=armv8.1-a -mcpu=thunderx2t99
```

### x86

The following options are used:

```
-O3 -march=core-avx2
```
