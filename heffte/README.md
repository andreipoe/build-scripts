# HeFFTe


This downloads HeFFTe from [their website](https://github.com/icl-utk-edu/heffte).
The current release used is 2.4.1.

To download sources:

    ./fetch.sh

There is a single command to build all version of the library

```bash
./build.sh /path/to/sources /path/to/prefix
PREFIX=/path/to/prefix CXX=armclang++ ./build.sh /path/to/sources
```

**Note**: This script will _automatically run `make install`_.

