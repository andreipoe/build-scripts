#!/bin/bash

set -eu
set -o pipefail

HEFFTE_VERSION="${HEFFTE_VERSION:-2.4.1}"
SHA256="${SHA256:-de2cf26df5d61baac7841525db3f393cb007f79612ac7534fd4757f154ba3e6c}"

# Fetch
tarname="v${HEFFTE_VERSION}.tar.gz"

if [ ! -r "$tarname" ]; then
    url="https://github.com/icl-utk-edu/heffte/archive/refs/tags/$tarname"
    wget "$url"
fi

actual_sha256="$(sha256sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha256" != "$SHA256" ]; then
    echo "Checksum does not match."
    echo "$SHA256 expected"
    echo "$actual_sha256 computed"
    exit 1
fi

tar xf "$tarname"

echo 'Now create a build directory and run `build.sh .. PREFIX`'
