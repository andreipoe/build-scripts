#!/bin/bash

set -eu
set -o pipefail

if [ $# -lt 2 ]; then
    echo "Usage: build.sh /path/to/sources /path/to/prefix"
    exit 1
fi
src="$1"
prefix="$2"

echo "Using prefix: $prefix"

if [ -n "${ARCH_OPTS:-}" ]; then
    mapfile -t arch_opts <<< "$ARCH_OPTS"
fi


cd "$src"

read -p "This will run 'make install'. Continue with PREFIX=$prefix? [y/N] " -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    echo "Abort."
    exit 2
fi

cmake \
    -D CMAKE_CXX_COMPILER="${CXX:-CC}" \
    -D CMAKE_CXX_FLAGS="-march=znver4 -Ofast -I${MPICH_DIR}/include ${arch_opts[*]}" \
    -D CMAKE_BUILD_TYPE=Release \
    -D BUILD_SHARED_LIBS=ON     \
    -D Heffte_ENABLE_AVX=ON \
    -D Heffte_ENABLE_AVX512=ON \
    -D Heffte_ENABLE_FFTW=ON \
    -D FFTW_ROOT="$FFTW_ROOT" \
    -D Heffte_ENABLE_ROCM=ON \
    -D CMAKE_HIP_COMPILER=CC \
    -D CMAKE_INSTALL_PREFIX="$prefix" \
    "$src"
make -j "${BUILD_THREADS:-16}"
make install

echo
echo "Done."
