# Open MPI

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
```

This downloads Open MPI from [their website](https://www.open-mpi.org/software/ompi/v4.0/). The current release used is 4.1.6.

## Build 

Set `FC`, `CC`, and `CXX` to specify compilers.

```bash
./build.sh /sources/dir/ /install/dir/
FC=armflang CC=armclang CXX=armclang++ ./build.sh /sources/dir/ /install/dir/
```

Note that you _need to_ specify all the script parameters above.

The following options are used by default:

```
--with-hwloc=internal --without-verbs --enable-orterun-prefix-by-default --enable-wrapper-rpath
```

If your system has optional packages installed, specifiy them using EXTRA_OPTS:

```
--with-knem=... --with-xpmem=... --with-hcoll=... --with-ofi=...
```

Consider also setting the name of your IB device in the modulefile, e.g.:

```
setenv UCX_NET_DEVICES mlx5_0:1
```
