#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

export BUILD_THREADS="${BUILD_THREADS:-32}"
export PLATFORM_OPTS="--with-ucx --with-slurm --with-pmi --without-tm${EXTRA_OPTS+ $EXTRA_OPTS}"

"${script_dir}/build.sh" "$@"
