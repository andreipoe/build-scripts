#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

export CC="${CC:-nvc}"
export CXX="${CXX:-nvc++}"
export FC="${FC:-nvfortran}"
export F90="${F90:-nvfortran}"

export CFLAGS="${CFLAGS:--fPIC -tp=zen3}"
export CXXFLAGS="${CXXFLAGS:--fPIC -march=znver3}"
export FCFLAGS="${FCFLAGS:--fPIC -march=znver3}"

export BUILD_THREADS="${BUILD_THREADS:-32}"
export PLATFORM_OPTS=" --with-cray-xpmem --with-ofi=/opt/cray/libfabric/1.11.0.4.128 --with-cuda --with-slurm --with-pmi --with-pmix --without-tm${EXTRA_OPTS+ $EXTRA_OPTS}"

"${script_dir}/build.sh" "$@"
