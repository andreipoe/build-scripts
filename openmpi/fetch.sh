#!/bin/bash

set -eu

#ompiver="3.0.0"; sha1sum="fa63990db1aec88b36335a1d79abf47ac6348e23"
#ompiver="3.0.3"; sha1sum="2e5e0e9b4911e762a5d66d4a437aca6bef66c1c7"
#ompiver="3.1.1"; sha1sum="86bc99c79a2c69a7d9b624d7f012929db78fc9a4"
#ompiver="3.1.5"; sha1sum="56a74b116c81d4f3704c051a67e4422094ff913d"
#ompiver="3.1.6"; sha1sum="bc4cd7fa0a7993d0ae05ead839e6056207e432d4"
#ompiver="4.0.2"; sha1sum="32ce3761288575fb8e4f6296c9105c3a25cf3235"
#ompiver="4.0.3"; sha1sum="d958454e32da2c86dd32b7d557cf9a401f0a08d3"
#ompiver="4.0.4"; sha1sum="50861c22a4b92ca2e069cd49d756dd96c659bfa8"
#ompiver="4.1.4"; sha1sum="357c61a8e06e103d987c0e4a054e8780a034c8b1"
ompiver="4.1.6"; sha1sum="4c3b5472140df96e71d8ac4e20106a62bede20f6"
targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

ompitar="openmpi-${ompiver}.tar.bz2"
wget "https://download.open-mpi.org/release/open-mpi/v${ompiver%.*}/${ompitar}"

actualh=$(sha1sum "$ompitar" | awk '{print $1}')
if [ "$actualh" != "$sha1sum" ]; then
    echo "Checksum does NOT match for: $ompitar"
    echo "Expected: $sha1sum"
    echo "Got: $actualh"
    exit 3
fi

if [ -d "openmpi-$ompiver" ]; then
    echo "Cannot extract: directory already exists: openmpi-$ompiver" >&2
    exit 4
fi

echo "Extrcting $ompitar..."
tar xf "$ompitar"

echo "Done."

