# Protocol Buffers

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
VERSION=3.12.3 ./fetch.sh
```

This downloads [protobuf from GitHub](https://github.com/protocolbuffers/protobuf/releases).
The current release used is 3.12.3.
Set `VERSION` to download a different version.

## Build 

```bash
./build.sh /sources/dir/ /install/dir/

```

Note that you _need to_ specify all the script parameters above.

No special options need to be used.

