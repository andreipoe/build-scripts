#!/bin/bash

set -eu

targetdir="$PWD"
version="${VERSION:-3.12.3}"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

wget "https://github.com/protocolbuffers/protobuf/releases/download/v${version}/protobuf-cpp-${version}.zip"

if [ -z "${VERSION:-}" ]; then
    sha256_expected="74da289e0d0c24b2cb097f30fdc09fa30754175fd5ebb34fae4032c6d95d4ce3"
    sha256_actual="$(sha256sum "cmake-${version}.tar.gz" | awk '{print $1}')"

    if [[ "$sha256_expected" != "$sha256_actual" ]]; then
        echo "SHA-256 mismatch."
        echo "Expected: $sha256_expected"
        echo "Computed: $sha256_actual"
        echo "Stop."
        exit 3
    fi
fi

unzip "protobuf-cpp-${version}.zip"

echo "Done."

