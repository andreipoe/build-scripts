#!/bin/bash

set -eu
set -o pipefail

SST_VERSION="${SST_VERSION:-8.0.0}"
SHA1_CORE="${SHA1_CORE:-44ddee9bb0957bc768a0cf4c5fce674fa2829308}"
SHA1_ELEMENTS="${SHA1_ELEMENTS:-c270399d0503afa3d32c9d892394519568ee6d16}"

PIN_VERSION="${PIN_VERSION:-2.14}"
SHA256_PIN="${SHA256_PIN:-1c29f589515772411a699a82fc4a3156cad95863a29741dfa6522865d4d281a1}"

# PIN
tarname="pin-2.14-71313-gcc.4.4.7-linux.tar.gz"
echo "Downloading $tarname..."
url="https://software.intel.com/sites/landingpage/pintool/downloads/$tarname"
wget "$url"
actual_sha256="$(sha256sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha256" != "$SHA256_PIN" ]; then
    echo "PIN checksum does not match."
    echo "$SHA256_PIN expected"
    echo "$actual_sha256 computed"
    exit 1
fi


# Core
tarname="sstcore-$SST_VERSION.tar.gz"
echo "Downloading $tarname..."
url="https://github.com/sstsimulator/sst-core/releases/download/v${SST_VERSION}_Final/$tarname"
wget "$url"
actual_sha1="$(sha1sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha1" != "$SHA1_CORE" ]; then
    echo "Core checksum does not match."
    echo "$SHA1_CORE expected"
    echo "$actual_sha1 computed"
    exit 1
fi

tar xf "$tarname"

# Elements
tarname="sstelements-$SST_VERSION.tar.gz"
echo "Downloading $tarname..."
url="https://github.com/sstsimulator/sst-elements/releases/download/v${SST_VERSION}_Final/$tarname"
wget "$url"
actual_sha1="$(sha1sum "$tarname" | awk '{print $1}')"

if [ "$actual_sha1" != "$SHA1_ELEMENTS" ]; then
    echo "Elements checksum does not match."
    echo "$SHA1_ELEMENTS expected"
    echo "$actual_sha1 computed"
    exit 1
fi

cd "sstcore-${SST_VERSION}"
tar xf "$tarname"

echo "Done."

