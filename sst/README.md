# SST

## Fetch

```bash
./fetch.sh # Uses `$PWD`
```

This downloads SST from [the releases page](http://sst-simulator.org/SSTPages/SSTMainDownloads/). The current release used is 8.0.0, but this can be overridden:

```bash
SST_VERSION=7.2.0 SHA1_CORE=6e0d156895241c108456b633e30a7509df700335 SHA1_ELEMENTS=ef353026b961ce84616bbb69d2b6e55517bf34b3 ./fetch.sh
```

## Build 

1. Build SST Core:
    ```bash
    ./build-core.sh # Builds for `$PROJ`
    ./build-core.sh /path/to/prefix
    PREFIX=/path/to/prefix ./build-core.sh
    ```

2. If desired, build SST Elements (requires Core to be `install`ed first):
    ```bash
    ./build-elements.sh # Builds for `$PROJ`
    ./build-elements.sh /path/to/prefix
    PREFIX=/path/to/prefix ./build-elements.sh
    ```

3. If desided, build Juno (which is built in its final destination directory, and required Core and Elements to be `install`ed first):
    ```bash
    ./build-juno.sh # Builds for `$PROJ`
    ./build-juno.sh /path/to/prefix
    PREFIX=/path/to/prefix ./build-juno.sh
    ```

### Build notes

- The following options are used:
    ```
    CFLAGS=-fpermissive
    ```
- On x86, PIN in unpacked and used to build Elements with Ariel support.

