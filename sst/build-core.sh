#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules/sst/8.0.0/}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"
CORE_PREFIX="${CORE_PREFIX:-$PREFIX/core}"

module purge
module load python/2.7.15
module load openmpi/gcc/64/1.10.1

export SST_CORE_HOME="$CORE_PREFIX"

echo "Building Core..."
CXXFLAGS='-fpermissive' ./configure --prefix="$SST_CORE_HOME"
make -j16

cat <<EOF

To install to "$CORE_PREFIX":
    make install

Next, to install SST Elements:
    ./build-elements.sh

Done.
EOF

