#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules/sst/8.0.0/}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"
CORE_PREFIX="${CORE_PREFIX:-$PREFIX/core}"
ELEMENTS_PREFIX="${ELEMENTS_PREFIX:-$PREFIX/elements}"
PIN_PREFIX="${PIN_PREFIX:-$PREFIX/..}"

module purge
module load python/2.7.15
module load openmpi/gcc/64/1.10.1

machine="$(uname -m)"
pin_flag=""
if [ "$machine" = "x86_64" ]; then
    echo "Unarchiving PIN..."
    tar -xf pin-2.14-71313-gcc.4.4.7-linux.tar.gz -C "$PIN_PREFIX"
    pin_flag="--with-pin=$PIN_HOME"
else
    echo "Not unarchiving PIN on non-x86 platform: $machine"
fi

export SST_CORE_HOME="$CORE_PREFIX"
export SST_ELEMENTS_HOME="$ELEMENTS_PREFIX"
export PIN_HOME="$PIN_PREFIX/pin-2.14-71313-gcc.4.4.7-linux"

echo "Building Elements..."
cd sst-elements-library-8.0.0
CXXFLAGS='-fpermissive' eval ./configure --prefix="$SST_ELEMENTS_HOME" --with-sst-core="$SST_CORE_HOME" "$pin_flag"
make -j16

cat <<EOF

To install to "$ELEMENTS_PREFIX":
    make install

Next, to install Juno:
    ./build-juno.sh

Done.
EOF

