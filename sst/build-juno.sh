#!/bin/bash

set -eu
set -o pipefail

PREFIX="${PREFIX:-/lustre/projects/bristol/modules/sst/8.0.0/}"

[ $# -ge 1 ] && PREFIX="$1"

echo "Using prefix: $PREFIX"
JUNO_PREFIX="${JUNO_PREFIX:-$PREFIX/juno}"

module purge
module load sst

echo "Building Juno..."
cd "$(dirname "$JUNO_PREFIX")"
git clone --depth 1 https://github.com/sstsimulator/juno.git

cd juno/src
make -j16

cd ../asm
make -j16

cat <<EOF

Done.
EOF

