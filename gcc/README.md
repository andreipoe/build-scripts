# GCC

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
VERSION=11.3.0 ./fetch.sh /path/to/download/dir
```

This downloads GCC from [the UK mirror](ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/). The current release used is 13.3.0.

## Build 

```bash
CC=gcc ./build.sh /sources/dir/ /build/dir/ /install/dir/
```

Note that you _need to_ specify all the script parameters above. The default compiler is `gcc`.

This builds a generic set-up that should be compatible with any platform.
To use platform-specific options:

```bash
CC=gcc BUILD_THREADS=64 PLATFORM_OPTS="--with-arch=armv8.3-a --with-cpu=a64fx" ./build.sh /sources/dir/ /build/dir/ /install/dir/

```

There is also a convenience script for common platforms:

```bash
PLATFORM=milan ./build.sh /sources/dir/ /build/dir/ /install/dir/
```

### AArch64

The following options are used:

```
--enable-languages=c,c++,fortran,lto --disable-bootstrap --disable-multilib --with-arch=armv8.1-a --with-cpu=thunderx2t99 --enable-default-pie
```

The build produced supports the SVE target.

