#!/bin/bash

set -eu

gccver="${VERSION:-13.3.0}"
targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

rm -f sha512.sum

[ ! -f "gcc-${gccver}.tar.xz" ] && wget ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-"$gccver"/gcc-"$gccver".tar.xz
wget ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-"$gccver"/sha512.sum

if ! grep -q -f <(sha512sum gcc-"$gccver".tar.xz) sha512.sum; then
    echo "Checksum does NOT match for: gcc-$gccver.tar.xz"
    exit 3
fi

if [ -d "gcc-$gccver" ]; then
    echo "Cannot extract: directory already exists: gcc-$gccver" >&2
    exit 4
fi

echo "Extrcting gcc-$gccver.tar.xz..."
tar xf gcc-$gccver.tar.xz

echo "Downloading prerequisites..."
cd gcc-$gccver
./contrib/download_prerequisites

echo "Done."

