#!/bin/bash

set -eu

CC="${CC:-gcc}"
BUILD_THREADS="${BUILD_THREADS:-16}"
IFS=' ' read -r -a PLATFORM_OPTS <<<"${PLATFORM_OPTS:-}"


if [ $# -ge 3 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh <source-dir> <build-dir> <install-dir>." >&2
        exit 1
    fi

    sourcedir="$(readlink -m "$1")"
    builddir="$(readlink -m "$2")"
    installdir="$(readlink -m "$3")"
else
    echo "Usage: build.sh <source-dir> <build-dir> <install-dir>." >&2
    exit 1
fi


if [ ! -d "$sourcedir" ]; then
    echo "Source directory does not exist: $sourcedir" >&2
    exit 2
fi

[ ! -d "$builddir" ] && mkdir "$builddir"

echo "Build directory: $builddir"
echo "Install directory: $installdir"

cd "$builddir"

"$sourcedir/configure" --prefix="$installdir" --enable-languages=c,c++,fortran,lto --disable-bootstrap --disable-multilib --enable-default-pie "${PLATFORM_OPTS[@]}"

time make -j "$BUILD_THREADS" |& tee build.log
make install

echo "Done."

