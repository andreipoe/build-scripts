#!/bin/bash

set -eu

if [ -z "${PLATFORM:-}" ]; then
    echo "Usage: PLATFORM=... build-with-arch.sh /sources/dir /build/dir /install/dir"
    exit 1
fi

export BUILD_THREADS="${BUILD_THREADS:-16}"

case "$PLATFORM" in
    a64fx)
        export PLATFORM_OPTS="--with-arch=a64fx --with-cpu=a64fx"
        ;;
    ampere|neoverse-n1|n1)
        export PLATFORM_OPTS="--with-arch=neoverse-n1 --with-cpu=neoverse-n1"
        ;;
    broadwell|bdw)
        export PLATFORM_OPTS="--with-arch=broadwell"
        ;;
    icelake|icl)
        export PLATFORM_OPTS="--with-arch=icelake-server"
        ;;
    milan)
        export PLATFORM_OPTS="--with-arch=znver3"
        ;;
    thunderx2|tx2)
        export PLATFORM_OPTS="--with-arch=thunderx2t99 --with-cpu=thunderx2t99"
        ;;
    znver*)
        export PLATFORM_OPTS="--with-arch=$PLATFORM"
        ;;
    *)
        echo "WARNING: Passing through unrecognised arch: $PLATFORM"
        export PLATFORM_OPTS="--with-arch=$PLATFORM"
        ;;
esac


script_dir="$(realpath "$(dirname "$(realpath "$0")")")"
"${script_dir}/build.sh" "$@"
