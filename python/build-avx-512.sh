#!/bin/bash

script_dir="$(realpath "$(dirname "$(realpath "$0")")")"

CC="${CC:-gcc}" CFLAGS="${CFLAGS:--O3 -march=icelake-server -fPIC}"  BUILD_THREADS="${BUILD_THREADS:-32}" "${script_dir}/build.sh" "$@"
