#!/bin/bash

PYTHON_VERSION="${PYTHON_VERSION:-3.9.2}"
BUILD_THREADS="${BUILD_THREADS:-32}"
IFS=' ' read -r -a PLATFORM_OPTS <<<"${PLATFORM_OPTS:-}"

usage () {
    echo "Usage: CC=gcc CFLAGS=... build.sh <install-dir>." >&2
}

set -eu
set -o pipefail

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        usage
        exit 1
    fi

    installdir="$(readlink -m "$1")"
else
    usage
    exit 1
fi

echo "Using prefix: $installdir"

./configure --prefix="$installdir" --enable-optimizations --enable-shared "${PLATFORM_OPTS[@]}"

make -j "$BUILD_THREADS"

cat <<EOF

To install to "$installdir":
    make install

Done.
EOF
