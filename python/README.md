# Python 3

## Fetch

```bash
./fetch.sh # Uses `$PWD`
```

This downloads Python from [source releases](https://www.python.org/downloads/source/). The current release used is 3.12.1, but this can be overridden:

```bash
PYTHON_VERSION=3.6.9 MD5=e229451dcb4f2ce8b0cac174bf309e0a ./fetch.sh
```

## Build 

```bash
./build.sh # Builds for `$PROJ`
./build.sh /path/to/prefix
PREFIX=/path/to/prefix ./build.sh
```

### ThunderX2

The following options are used:

```
-O3 -march=armv8.1-a -mcpu=thunderx2t99
```

### x86

The following options are used:

```
-O3 -march=core-avx2
```
