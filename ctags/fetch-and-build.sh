#!/bin/bash

set -eu
set -o pipefail

if [ $# -lt 1 ]; then
    echo "Usage: fetch-and-build.sh /path/to/install/dir"
    exit 1
fi

version="${VERSION:-master}"
prefix="$1"

echo "Installing to: $prefix"

if [ -d ctags ]; then
    cd ctags
    git pull
else
    git clone https://github.com/universal-ctags/ctags.git
    cd ctags
fi
git checkout "$version"

./autogen.sh
./configure --prefix="$prefix"
make -j
make install

echo "Done."
