# ctags

This builds [Universal Ctags](https://github.com/universal-ctags/ctags.git).
There are no releases, so the latest commit is used.
The source is downloaded in the current directory.

```
./fetch-and-build.sh /path/to/install/dir # Uses master
VERSION=deadbeef ./fetch-and-build.sh /path/to/install/dir
```

## vim-easytags

As of 11-Feb-2020, vim-easytags doesn't recognise universal-ctags properly.
To work around it, `git apply universal-ctags.patch` from `~/.vim/bundle/vim-easytags`.
The patch is taken from [a GitHub issue](https://github.com/xolox/vim-easytags/pull/133#issuecomment-316477649).
