#!/bin/bash

set -eu

if [ $# -ge 2 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: build.sh <source-dir> <install-dir>." >&2
        exit 1
    fi

    sourcedir="$(readlink -f "$1")"
    installdir="$(readlink -m "$2")"
else
    echo "Usage: build.sh <source-dir> <install-dir>." >&2
    exit 1
fi


if [ ! -d "$sourcedir" ]; then
    echo "Source directory does not exist: $sourcedir" >&2
    exit 2
fi

echo "Install directory: $installdir"

cd "$sourcedir"

rm -rf build && mkdir build
cd build

if hash cmake 2>/dev/null; then
    echo "Found existing cmake install..."
    cmake -DCMAKE_INSTALL_PREFIX="$installdir" -DCMAKE_BUILD_TYPE=Release ..
else
    echo "Bootstrapping cmake..."
    ../bootstrap --prefix="$installdir" -- -DCMAKE_BUILD_TYPE:STRING=Release
fi

time make -j |& tee build.log
make install

echo "Done."

