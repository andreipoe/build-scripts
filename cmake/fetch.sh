#!/bin/bash

set -eu

targetdir="$PWD"
version="${VERSION:-3.17.3}"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

wget "https://github.com/Kitware/CMake/releases/download/v${version}/cmake-${version}.tar.gz"

sha256_expected="$(curl -sSL "https://github.com/Kitware/CMake/releases/download/v${version}/cmake-${version}-SHA-256.txt" | awk "/cmake-${version}.tar.gz/"' {print $1}')"
sha256_actual="$(sha256sum "cmake-${version}.tar.gz" | awk '{print $1}')"

if [[ "$sha256_expected" != "$sha256_actual" ]]; then
    echo "SHA-256 mismatch."
    echo "Expected: $sha256_expected"
    echo "Computed: $sha256_actual"
    echo "Stop."
    exit 3
fi

tar xf "cmake-${version}.tar.gz"

echo "Done."

