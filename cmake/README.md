# CMake

## Fetch

```bash
./fetch.sh /path/to/download/dir # default is `$PWD`
VERSION=3.14.10 ./fetch.sh
```

This downloads [CMake from their website](https://cmake.org/download/).
The current release used is 3.16.4.
Set `VERSION` to download a different version.

## Build 

```bash
./build.sh /sources/dir/ /install/dir/

```

Note that you _need to_ specify all the script parameters above.

No special options need to be used.

