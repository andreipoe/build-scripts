# ARMIE

Both scripts take `-h` or `--help` to show the usage.

## Fetch

```bash
./fetch.sh /path/to/target # default is $PWD
```

This will download ARMIE from [the downloads page](https://developer.arm.com/products/software-development-tools/hpc/downloads/download-arm-instruction-emulator), then unarchive it. The current version use is 18.1 for SUSE.

## Build

```bash
./install.sh /path/to/install/dir
./install.sh /path/to/install/dir /path/to/installer # Default is what fetch.sh downloaded into $PWD
```

This will run the ArmIE installer and update the modulefiles to replace the default Arm names with our own naming convention.

