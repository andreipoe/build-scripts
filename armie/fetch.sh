#!/bin/bash

set -eu

targetdir="$PWD"
armiever="18.1"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"

fname="armie-$armiever.tar"
outfile="$targetdir/$fname"
if [ -f "$outfile" ]; then
    echo "File already exists: $outfile" >&2
    exit 4
fi

wget -O "$outfile" https://developer.arm.com/-/media/Files/downloads/hpc/Arm%20Instruction%20Emulator%20downloads/18-1/ARM-Instruction-Emulator_18.1_AArch64_SUSE_12_aarch64.tar?revision=3001d9cc-ab49-4dec-91ff-6dd0d9c43c09

expectedh="2fb0c67eff5e3264220ce6443d08f7c6bde130c51199597f9e1c4dc111f9dffc"
actualh=$(sha256sum "$outfile" | awk '{print $1}')
if [ "$actualh" != "$expectedh" ]; then
    echo "Checksum does NOT match for: $outfile"
    echo "Expected: $expectedh"
    echo "Actual: $actualh"
    exit 3
fi

echo "Unarchving armie $armiever..."
cd "$targetdir"
tar xvf "$fname"
tar xvf "ARM-Instruction-Emulator_${armiever}_AArch64_SUSE_12_aarch64.tar.gz"

echo "Done."

