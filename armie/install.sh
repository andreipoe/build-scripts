#!/bin/bash

set -eu

installer="ARM-Instruction-Emulator_18.1_AArch64_SUSE_12_aarch64/arm-instruction-emulator-18.1_Generic-AArch64_SUSE-12_aarch64-linux-rpm.sh"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: install.sh <install-dir> [<installer>]." >&2
        exit 1
    fi

    installdir="$(readlink -f "$1")"
    [ $# -ge 2 ] && installer="$(readlink -f "$2")"
else
    echo "Usage: install.sh <install-dir> [<installer>]." >&2
    exit 1
fi


if [ ! -f "$installer" ]; then
    echo "Installer does not exist: $installer" >&2
    echo "Have you run fetch.sh?" >&2
    exit 2
fi

[ ! -d "$installdir" ] && mkdir "$installdir"

echo "Using installer: $installer"
echo "Install directory: $installdir"

echo "Installing ArmIE..."
"$installer" --install-to "$installdir"

echo ""
echo "Adjusting modulefiles..."
cd "$installdir/opt/arm/modulefiles"

# Reorganise the direcotry sturcture to get rid of Generic-AArch64/...
mkdir -p arm/{instruction-emulator,gcc-runtimes}
mv Generic-AArch64/SUSE/12/arm-instruction-emulator/18.1 arm/instruction-emulator/
mv Generic-AArch64/SUSE/12/gcc_runtimes/7.1.0 arm/gcc-runtimes/7.1.0-armie-18.1
rm -r Generic-AArch64/

# Adjust the paths inside the modules themselves
sed -i 's,set this_module_path        .*,set this_module_path        arm/gcc-runtimes/7.1.0-armie-18.1,' arm/gcc-runtimes/7.1.0-armie-18.1
sed -i 's,set this_module_path        .*,set this_module_path        arm/instruction-emulator/18.1,' arm/instruction-emulator/18.1
sed -i 's, Generic-AArch64/.*\\, arm/gcc-runtimes/7.1.0-armie-18.1 \\,' arm/instruction-emulator/18.1

echo "To use the new installtion: module use $installdir/opt/arm/modulefiles"

echo "Done."

