# ScaLAPACK

The current version used is 2.2.0.

## Build

Set `CC` and `FC` to your MPI compilers

```bash
./build.sh /sources/dir/ /install/dir # Uses mpicc and mpif90
CC=cc FC=ftn ./build.sh /sources/dir/ /install/dir
```
