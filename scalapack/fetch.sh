#!/bin/bash

set -eu

slver="2.2.0"; sha256sum="8862fc9673acf5f87a474aaa71cd74ae27e9bbeee475dbd7292cec5b8bcbdcf3"
targetdir="$PWD"

if [ $# -ge 1 ]; then
    if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: fetch.sh [<target-dir>]." >&2
        exit 1
    fi

    if [ -d "$1" ]; then
        targetdir="$1"
    else
        echo "Selected directory does not exist: $1" >&2
        exit 2
    fi
fi

echo "Target directory: $targetdir"
cd "$targetdir"

https://github.com/Reference-ScaLAPACK/scalapack/archive/refs/tags/v2.2.0.tar.gz
sltar="v${slver}.tar.gz"
wget "https://github.com/Reference-ScaLAPACK/scalapack/archive/refs/tags/${slar}"

actualh=$(sha1sum "$sltar" | awk '{print $1}')
if [ "$actualh" != "$sha256sum" ]; then
    echo "Checksum does NOT match for: $sltar"
    echo "Expected: $sha256sum"
    echo "Got: $actualh"
    exit 3
fi

sldir="scalapack-$slver"
if [ -d  ]; then
    echo "Cannot extract: directory already exists: $sldir" >&2
    exit 4
fi

echo "Extrcting $sltar..."
tar xf "$sltar"

echo "Done."

